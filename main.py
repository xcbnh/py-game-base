# -*- coding: utf-8 -*-

from Fishing.src.main_window import MainWindow

# 启动入口
if __name__ == "__main__":
    main_window = MainWindow()
    main_window.run_game()