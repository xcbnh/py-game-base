# -*- coding: utf-8 -*-
"""
@FileName：config.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/7/22 22:00\n
"""
# pathlib 是 Python 3.4 引入的一个标准库，它提供了面向对象的文件系统路径操作。
from pathlib import Path

import pygame

pygame.font.init()

# 标题
WIN_TITLE = "小鱼爱上钩"

# 窗口大小
SCREEN_WIDTH = 900
SCREEN_HEIGHT = 500

# 每秒刷新帧数
FRASH_SPEED = 45

# 当前工作目录，以 main.py 为准
_current_path = Path(".").resolve()

# 图标
ICON_PATH = _current_path / "assets" / "images" / "icon.png"
# 字体
FONT_PATH = _current_path / "assets" / "fonts" / "Alibaba-PuHuiTi-Regular.ttf"
# 背景
BACKGROUND_IMG_PATH = _current_path / "assets" / "images" / "bg.png"
PUAS_PATH = _current_path / "assets" / "images" / "puas.png"
# 鱼钩
FISH_HOOK_PATH = _current_path / "assets" / "images" / "fishhook.png"
# 鱼
FISH_PATH_1 = _current_path / "assets" / "images" / "fish_1.png"
FISH_PATH_2 = _current_path / "assets" / "images" / "fish_2.png"
FISH_PATH_3 = _current_path / "assets" / "images" / "fish_3.png"
FISH_PATH_4 = _current_path / "assets" / "images" / "fish_4.png"
FISH_PATH_5 = _current_path / "assets" / "images" / "fish_5.png"
FISH_PATH_6 = _current_path / "assets" / "images" / "fish_6.png"
FISH_PATH_7 = _current_path / "assets" / "images" / "fish_7.png"
FISH_PATH_8 = _current_path / "assets" / "images" / "fish_8.png"

# 背景音乐
BGM_PATH_1 = _current_path / "assets" / "sounds" / "bgm1.mp3"
BGM_PATH_2 = _current_path / "assets" / "sounds" / "bgm2.mp3"
BGM_PATH_3 = _current_path / "assets" / "sounds" / "bgm3.mp3"
# 气泡
BUBBLE_PATH_1 = _current_path / "assets" / "sounds" / "bubble_1.WAV"
BUBBLE_PATH_2 = _current_path / "assets" / "sounds" / "bubble_2.WAV"
BUBBLE_PATH_3 = _current_path / "assets" / "sounds" / "bubble_3.WAV"
BUBBLE_PATH_4 = _current_path / "assets" / "sounds" / "bubble_4.WAV"
BUBBLE_PATH_5 = _current_path / "assets" / "sounds" / "bubble_5.WAV"
# 升级
LEVEUP_PATH = _current_path / "assets" / "sounds" / "levelup.WAV"
# 菜单
MENU_PATH = _current_path / "assets" / "sounds" / "menu.WAV"

# 颜色
BUTTON_COLOR = (247, 181, 0)
TILTE_COLOR = (247, 181, 0)
FONT_COLOR = (255, 255, 255)
BLACK_COLOR = (0, 0, 0)
MEAU_COLOR = (0, 0, 0)

#  语言
WORDS_DICT = {
    "小鱼爱上钩": "Fish Love Hook",
    "最高分": "Highest marks",
    "最高等级": "Top level",
    "继续游戏": "Continue",
    "开始新游戏": "Start",
    "设置": "Settings",
    "难度": "Diff",
    "音量": "Vol",
    "亮度": "Illumi",
    "语言": "Lang",
    "返回": "Home",
    "暂停": "Pause",
    "绳索强度": "Rope Strength",
    "等级LV": "Level"
}

# 字体大小
FONT_SIZE_12 = pygame.font.Font(FONT_PATH, 12)
FONT_SIZE_18 = pygame.font.Font(FONT_PATH, 18)
FONT_SIZE_20 = pygame.font.Font(FONT_PATH, 20)
FONT_SIZE_24 = pygame.font.Font(FONT_PATH, 24)
FONT_SIZE_36 = pygame.font.Font(FONT_PATH, 36)

# 关卡
GAME_SEED = [{
    "level": 1,
    "seed": 1,
    "strength": 1,
    "fish_num": 4
}]
