# -*- coding: utf-8 -*-
"""
@FileName：main_window.py
@Description：创建 pygame 主窗口，刷新页面，检测鼠标点击事件
@Author：锦沐Python
@Time：2024/8/17 16:26
"""

import pygame

from Fishing.src.sys.control_sys import ControlSys
from Fishing.src.config.config import SCREEN_WIDTH, SCREEN_HEIGHT
from Fishing.src.config.config import ICON_PATH, WIN_TITLE, FRASH_SPEED, BLACK_COLOR
from Fishing.src.sys.music_sys import music_sys

# 必须初始化
pygame.init()


class MainWindow():
    """
    负责主窗口初始化，加载控制系统，刷新页面，检测点击事件
    """
    def __init__(self):

        # 配置主窗口大小
        self.screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

        # 窗口标题
        pygame.display.set_caption(WIN_TITLE)
        # 图标
        icon = pygame.image.load(ICON_PATH).convert_alpha()
        pygame.display.set_icon(icon)

        # 用于控制刷新帧率
        self.clock = pygame.time.Clock()
        # 运行标志
        self.running_flag = True

        # 初始化画面控制器
        self.control_sys = ControlSys(self.screen)

    def run_game(self):
        # 播放背景音乐
        music_sys.play_bgm(True)
        # 循环处理页面
        while self.running_flag:
            # 配置刷新率为 60 帧
            self.clock.tick(FRASH_SPEED)

            # 事件处理
            for event in pygame.event.get():
                # 退出程序事件
                if event.type == pygame.QUIT:
                    self.running_flag = False
                    # 关闭背景音乐
                    music_sys.play_bgm(False)
                # 鼠标事件
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    # 检测左键点击
                    if event.button == pygame.BUTTON_LEFT:
                        self.control_sys.click_event(pos=event.pos)

            # 清空画面
            self.screen.fill(BLACK_COLOR)
            # 更新页面
            self.control_sys.draw_ui()
            # 刷新页面
            pygame.display.flip()

        # 退出程序
        pygame.quit()
