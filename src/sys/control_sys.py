# -*- coding: utf-8 -*-
"""
@FileName：control_sys.py
@Description：游戏页面管理，组装游戏画面
@Author：锦沐Python
@Time：2024/8/17 16:26
"""
from Fishing.src.config.config import *
from Fishing.src.sys.music_sys import music_sys
from Fishing.src.sys.bubbles_sys import BubblesSys
from Fishing.src.sys.player_sys import PlayerSys
from Fishing.src.sys.fish_sys import FishSys
from Fishing.src.sys.crash_sys import crash_sys


class ControlSys:
    """
    游戏页面管理，组装游戏画面
    """
    def __init__(self, screen=None):
        self.screen = screen
        # 语言
        self.language = "zh"
        # 当前页面
        self.current_page = "start"

        # 窗体大小
        self.screen_width = self.screen.get_width()
        self.screen_height = self.screen.get_height()
        # 按钮之间间隔
        self.v_margin = 50
        self.h_margin = 45

        # 菜单栏位置
        self.menu_width = 280
        self.menu_height = 380
        self.menu_pos_x = (self.screen_width - self.menu_width) // 2
        self.menu_pos_y = (self.screen_height - self.menu_height) // 2

        # 初始化参数
        self._start_init()
        self._settings_init()
        self._pause_init()
        self._running_init()

        # 初始化
        self.fish_sys = FishSys(screen=self.screen)
        self.player_sys = PlayerSys(screen=self.screen, start_position=(SCREEN_WIDTH // 2, 10))

        self.bubble_sys = BubblesSys(screen=self.screen, max_num=10, start_center_pos=(SCREEN_WIDTH, SCREEN_HEIGHT))

    def _start_init(self):
        # 位置
        self.start_menu_title_pos_x = self.screen_width // 2
        self.start_menu_title_pos_y = self.screen_height // 4

        self.start_menu_continued_button_width = 180
        self.start_menu_continued_button_height = 38

        self.start_menu_continued_button_pos_x = (self.screen_width - self.start_menu_continued_button_width) // 2
        self.start_menu_continued_button_pos_y = (self.screen_height - self.start_menu_continued_button_height) // 2 + self.h_margin

        self.start_menu_new_button_width = 180
        self.start_menu_new_button_height = 38

        self.start_menu_new_button_pos_x = (self.screen_width - self.start_menu_new_button_width) // 2
        self.start_menu_new_button_pos_y = (self.screen_height - self.start_menu_new_button_height) // 2 + self.h_margin * 2

        self.start_menu_settings_button_width = 180
        self.start_menu_settings_button_height = 38

        # 位置
        self.start_menu_settings_button_pos_x = (self.screen_width - self.start_menu_settings_button_width) // 2
        self.start_menu_settings_button_pos_y = (self.screen_height - self.start_menu_settings_button_height) // 2 + self.h_margin * 3

        # 继续游戏
        self.start_continue_button = pygame.Rect(self.start_menu_continued_button_pos_x,
                                                 self.start_menu_continued_button_pos_y,
                                                 self.start_menu_continued_button_width,
                                                 self.start_menu_continued_button_height)
        # 开始新游戏
        self.start_new_button = pygame.Rect(self.start_menu_new_button_pos_x,
                                            self.start_menu_new_button_pos_y,
                                            self.start_menu_new_button_width,
                                            self.start_menu_new_button_height)
        # 设置
        self.start_settings_button = pygame.Rect(self.start_menu_settings_button_pos_x,
                                                 self.start_menu_settings_button_pos_y,
                                                 self.start_menu_settings_button_width,
                                                 self.start_menu_settings_button_height)

    def _start_ui(self):
        # 菜单栏背景
        menu_bar = pygame.Surface((self.menu_width, self.menu_height))
        menu_bar.set_alpha(128)
        menu_bar.fill(MEAU_COLOR)
        self.screen.blit(menu_bar, (self.menu_pos_x, self.menu_pos_y))

        # 按钮
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.start_continue_button, border_radius=20)
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.start_new_button, border_radius=20)
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.start_settings_button, border_radius=20)

        # 文字
        # 标题
        title = FONT_SIZE_36.render(self.translated("小鱼爱上钩"), True, TILTE_COLOR)
        title_rect = title.get_rect()
        title_rect.center = (self.start_menu_title_pos_x,
                             self.start_menu_title_pos_y)
        self.screen.blit(title, title_rect)

        # 最高分
        hight_score = FONT_SIZE_24.render(self.translated("最高分") + f": {self.player_sys.max_score}", True, FONT_COLOR)
        hight_score_rect = hight_score.get_rect()
        hight_score_rect.center = (self.screen_width // 2,
                                   self.screen_height // 4 + self.h_margin * 2)
        self.screen.blit(hight_score, hight_score_rect)

        # 最高等级
        hight_level = FONT_SIZE_24.render(self.translated("最高等级") + f": {self.player_sys.max_level}", True, FONT_COLOR)
        hight_level_rect = hight_level.get_rect()
        hight_level_rect.center = (self.screen_width // 2,
                                   self.screen_height // 4 + self.h_margin * 2.5)
        self.screen.blit(hight_level, hight_level_rect)

        continue_font = FONT_SIZE_24.render(self.translated("继续游戏"), True, FONT_COLOR)
        continue_font_rect = continue_font.get_rect()
        continue_font_rect.center = (self.screen_width // 2,
                                     self.start_menu_continued_button_pos_y + self.start_menu_continued_button_height // 2)
        self.screen.blit(continue_font, continue_font_rect)

        new_font = FONT_SIZE_24.render(self.translated("开始新游戏"), True, FONT_COLOR)
        new_font_rect = new_font.get_rect()
        new_font_rect.center = (self.screen_width // 2,
                                self.start_menu_new_button_pos_y + self.start_menu_new_button_height // 2)
        self.screen.blit(new_font, new_font_rect)

        settings_font = FONT_SIZE_24.render(self.translated("设置"), True, FONT_COLOR)
        settings_font_rect = settings_font.get_rect()
        settings_font_rect.center = (self.screen_width // 2,
                                     self.start_menu_settings_button_pos_y + self.start_menu_settings_button_height // 2)
        self.screen.blit(settings_font, settings_font_rect)

    def _settings_init(self):
        # 亮度
        self.alpha = 0
        self.settings_bar_total = 150
        self.settings_difficulty_bar_left_w = 1
        self.settings_light_bar_left_w = 150
        self.settings_volume_bar_left_w = pygame.mixer_music.get_volume() * self.settings_bar_total

        self.en_color = FONT_COLOR
        self.cn_color = BUTTON_COLOR
        
        self.bar_start_pos_x = (self.screen_width - self.settings_bar_total) // 2 - 4 + self.h_margin//2
        self.font_start_pos_x = self.screen_width // 2 - 2*self.h_margin

        self.settings_difficulty_bar = pygame.Rect(self.bar_start_pos_x, self.screen_height // 2 - 3 - self.v_margin, self.settings_bar_total + 8, 12)
        self.settings_volume_bar = pygame.Rect(self.bar_start_pos_x, self.screen_height // 2 - 3, self.settings_bar_total + 8, 12)
        self.settings_light_bar = pygame.Rect(self.bar_start_pos_x, self.screen_height // 2 - 3 + self.v_margin, self.settings_bar_total + 8, 12)

        self.settings_home_button = pygame.Rect((self.screen_width - 200) // 2, self.screen_height // 2 + self.v_margin * 3 - 15, 200, 30)
        self.settings_en_button = pygame.Rect(self.bar_start_pos_x, self.screen_height // 2 + self.v_margin*2 - 15, 70, 30)
        self.settings_zh_button = pygame.Rect(self.bar_start_pos_x + self.h_margin * 2, self.screen_height // 2 + self.v_margin * 2 - 15, 70, 30)

    def _settings_ui(self):
        # 菜单栏背景
        menu_bar = pygame.Surface((self.menu_width, self.menu_height))
        menu_bar.set_alpha(128)
        menu_bar.fill(MEAU_COLOR)
        self.screen.blit(menu_bar, (self.menu_pos_x, self.menu_pos_y))

        # 按钮
        pygame.draw.rect(self.screen, self.en_color, self.settings_en_button, border_radius=20)
        pygame.draw.rect(self.screen, self.cn_color, self.settings_zh_button, border_radius=20)
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.settings_home_button, border_radius=20)
        # 数值条
        # 难度
        pygame.draw.rect(self.screen, FONT_COLOR, self.settings_difficulty_bar)
        pygame.draw.rect(self.screen, BUTTON_COLOR,
                         pygame.Rect(self.bar_start_pos_x + 4, self.screen_height // 2 - self.v_margin,
                                     self.settings_difficulty_bar_left_w, 6))
        # 音量
        pygame.draw.rect(self.screen, FONT_COLOR, self.settings_volume_bar)
        pygame.draw.rect(self.screen, BUTTON_COLOR,
                         pygame.Rect(self.bar_start_pos_x + 4, self.screen_height // 2, self.settings_volume_bar_left_w,
                                     6))
        # 暗度
        pygame.draw.rect(self.screen, FONT_COLOR, self.settings_light_bar)
        pygame.draw.rect(self.screen, BUTTON_COLOR,
                         pygame.Rect(self.bar_start_pos_x + 4, self.screen_height // 2 + self.v_margin,
                                     self.settings_light_bar_left_w, 6))

        # 文字
        # 标题
        title = FONT_SIZE_36.render(self.translated("设置"), True, TILTE_COLOR)
        title_rect = title.get_rect()
        title_rect.center = (self.start_menu_title_pos_x,
                             self.start_menu_title_pos_y)
        self.screen.blit(title, title_rect)

        bar_font = FONT_SIZE_20.render(self.translated("难度"), True, FONT_COLOR)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.font_start_pos_x, self.screen_height//2 - self.v_margin + 3)
        self.screen.blit(bar_font, bar_font_rect)

        bar_font = FONT_SIZE_20.render(self.translated("音量"), True, FONT_COLOR)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.font_start_pos_x, self.screen_height // 2 + 3)
        self.screen.blit(bar_font, bar_font_rect)

        bar_font = FONT_SIZE_20.render(self.translated("亮度"), True, FONT_COLOR)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.font_start_pos_x, self.screen_height // 2 + self.v_margin + 3)
        self.screen.blit(bar_font, bar_font_rect)

        bar_font = FONT_SIZE_20.render(self.translated("语言"), True, FONT_COLOR)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.font_start_pos_x, self.screen_height // 2 + self.v_margin*2)
        self.screen.blit(bar_font, bar_font_rect)

        bar_font = FONT_SIZE_18.render("English", True, self.cn_color)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.bar_start_pos_x + 35, self.screen_height // 2 + self.v_margin * 2)
        self.screen.blit(bar_font, bar_font_rect)

        bar_font = FONT_SIZE_18.render("中文", True, self.en_color)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.bar_start_pos_x + 80 + self.h_margin, self.screen_height // 2 + self.v_margin * 2)
        self.screen.blit(bar_font, bar_font_rect)

        # 返回
        bar_font = FONT_SIZE_24.render(self.translated("返回"), True, FONT_COLOR)
        bar_font_rect = bar_font.get_rect()
        bar_font_rect.center = (self.screen_width//2, self.screen_height // 2 + self.v_margin * 3)
        self.screen.blit(bar_font, bar_font_rect)

    def _pause_init(self):
        self.pause_menu_continued_button_width = 180
        self.pause_menu_continued_button_height = 38

        # 位置
        self.pause_menu_continued_button_pos_x = (self.screen_width - self.pause_menu_continued_button_width) // 2
        self.pause_menu_continued_button_pos_y = (self.screen_height - self.pause_menu_continued_button_height) // 2

        self.pause_menu_home_button_width = 180
        self.pause_menu_home_button_height = 38

        # 位置
        self.pause_menu_home_button_pos_x = (self.screen_width - self.pause_menu_home_button_width) // 2
        self.pause_menu_home_button_pos_y = (
                                                        self.screen_height - self.pause_menu_home_button_height) // 2 + self.v_margin

        # 继续游戏

        self.pause_continue_button = pygame.Rect(self.pause_menu_continued_button_pos_x,
                                                 self.pause_menu_continued_button_pos_y,
                                                 self.pause_menu_continued_button_width,
                                                 self.pause_menu_continued_button_height)
        # 返回主页
        self.pause_home_button = pygame.Rect(self.pause_menu_home_button_pos_x,
                                             self.pause_menu_home_button_pos_y,
                                             self.pause_menu_home_button_width,
                                             self.pause_menu_home_button_height)

    def _pause_ui(self):
        # 菜单栏背景
        menu_bar = pygame.Surface((self.menu_width, self.menu_height))
        menu_bar.set_alpha(128)
        menu_bar.fill(MEAU_COLOR)
        self.screen.blit(menu_bar, (self.menu_pos_x, self.menu_pos_y))

        # 按钮
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.pause_continue_button, border_radius=20)
        pygame.draw.rect(self.screen, BUTTON_COLOR, self.pause_home_button, border_radius=20)
        # 标题

        title = FONT_SIZE_36.render(self.translated("暂停"), True, TILTE_COLOR)
        title_rect = title.get_rect()
        title_rect.center = (self.start_menu_title_pos_x, self.start_menu_title_pos_y)
        self.screen.blit(title, title_rect)

        continue_font = FONT_SIZE_24.render(self.translated("继续游戏"), True, FONT_COLOR)
        continue_font_rect = continue_font.get_rect()
        continue_font_rect.center = (self.screen_width // 2,
                                     self.pause_menu_continued_button_pos_y + self.pause_menu_continued_button_height // 2)
        self.screen.blit(continue_font, continue_font_rect)

        home_font = FONT_SIZE_24.render(self.translated("返回"), True, FONT_COLOR)
        home_font_rect = home_font.get_rect()
        home_font_rect.center = (self.screen_width // 2,
                                 self.pause_menu_home_button_pos_y + self.pause_menu_home_button_height // 2)
        self.screen.blit(home_font, home_font_rect)

    def _running_init(self):
        self.running_pause_button = pygame.Rect(self.screen_width // 2, self.screen_height - self.v_margin, 30, 30)
        self.running_level_bar_total = 100
        self.running_level_bar_left_w = 10
        self.running_level_bar = pygame.Rect(self.screen_width-150, 40, self.running_level_bar_total + 8, 6)

    def _running_ui(self):
        text = FONT_SIZE_18.render(self.translated("绳索强度")+f":{self.player_sys.strength}", True, BLACK_COLOR)
        text_rect = text.get_rect()
        text_rect.topleft = (10, 10)
        self.screen.blit(text, text_rect)

        text = FONT_SIZE_18.render(self.translated("等级LV")+f":{self.player_sys.level}", True, FONT_COLOR)
        text_rect = text.get_rect()
        text_rect.topleft = (self.screen_width-150, 5)
        self.screen.blit(text, text_rect)

        self.running_level_bar_left_w = self.player_sys.score % 100
        pygame.draw.rect(self.screen, FONT_COLOR, self.running_level_bar, border_radius=3)
        pygame.draw.rect(self.screen, BUTTON_COLOR, pygame.Rect(self.screen_width-150, 40, self.running_level_bar_left_w, 6), border_radius=3)

        pygame.draw.rect(self.screen, BUTTON_COLOR, self.running_pause_button, border_radius=15)
        pygame.draw.rect(self.screen, BLACK_COLOR, (self.screen_width//2 + 10, self.screen_height - self.v_margin + 10, 2, 10), 0)
        pygame.draw.rect(self.screen, BLACK_COLOR, (self.screen_width//2 + 20, self.screen_height - self.v_margin + 10, 2, 10), 0)

    def translated(self, content):
        """
        翻译
        :param content:  原始内容
        :return: 翻译结果
        """
        if self.language == "zh":
            return content
        elif self.language == "en":
            return WORDS_DICT[content]

    def click_event(self, pos):
        """
        点击事件处理
        :param pos: 当前鼠标坐标，当前页面
        """
        # 点击音效
        music_sys.play_menu(True)

        if self.current_page == "start":
            if self.start_continue_button.collidepoint(pos):
                self.current_page = "running"

            elif self.start_new_button.collidepoint(pos):
                self.current_page = "running"
                self.player_sys.start_new()
                print("start_new_button")

            elif self.start_settings_button.collidepoint(pos):
                self.current_page = "settings"

        elif self.current_page == "pause":
            if self.pause_continue_button.collidepoint(pos):
                print("pause_continue_button")
                self.current_page = "running"

            elif self.pause_home_button.collidepoint(pos):
                self.current_page = "start"

        elif self.current_page == "running":
            # 设置玩家点击
            self.player_sys.set_animate_pos(pos=pos)
            if self.running_pause_button.collidepoint(pos):
                self.current_page = "pause"
                print("pause_continue_button")

        elif self.current_page == "settings":
            if self.settings_difficulty_bar.collidepoint(pos):
                print("settings_difficulty_bar")
                print(pos)
                (x, y) = pos
                self.settings_difficulty_bar_left_w = x - (self.screen_width - self.settings_bar_total) // 2 - self.h_margin//2
                self.current_page = "settings"

                self.fish_sys.min_eacape_probaility = min(self.settings_difficulty_bar_left_w/self.settings_bar_total, 1)

            elif self.settings_volume_bar.collidepoint(pos):
                print("settings_volume_bar")
                print(pos)
                (x, y) = pos
                self.settings_volume_bar_left_w = x - (self.screen_width - self.settings_bar_total) // 2 - self.h_margin//2
                self.current_page = "settings"
                print(self.settings_volume_bar_left_w / self.settings_bar_total)
                music_sys.volume_setting(volume=self.settings_volume_bar_left_w / self.settings_bar_total)

            elif self.settings_light_bar.collidepoint(pos):
                print("settings_light_bar")
                print(pos)
                (x, y) = pos
                self.settings_light_bar_left_w = x - (
                            self.screen_width - self.settings_bar_total) // 2 - self.h_margin // 2
                self.current_page = "settings"
                print(self.settings_light_bar_left_w / self.settings_bar_total)
                self.alpha = (1-self.settings_light_bar_left_w / self.settings_bar_total) * 255
                self.alpha = max(0, min(self.alpha, 255))  # 限制 alpha 在 0 到 255 的范围

            elif self.settings_en_button.collidepoint(pos):
                print("settings_en_button")
                self.en_color = BUTTON_COLOR
                self.cn_color = FONT_COLOR
                self.current_page = "settings"
                self.language = "en"

            elif self.settings_zh_button.collidepoint(pos):
                print("settings_zh_button")
                self.en_color = FONT_COLOR
                self.cn_color = BUTTON_COLOR
                self.current_page = "settings"
                self.language = "zh"

            elif self.settings_home_button.collidepoint(pos):
                print("settings_home_button")
                self.current_page = "start"

        print(self.current_page)

    def draw_bg(self):
        """
        绘制背景图片
        """
        background_image = pygame.image.load(BACKGROUND_IMG_PATH).convert()
        background_image = pygame.transform.smoothscale(background_image, (self.screen_width, self.screen_height))
        self.screen.blit(background_image, (0, 0))

    def draw_fog(self):
        """
        创建一个与原图像大小相同有透明度的全黑色图层
        """
        fog_surface = pygame.Surface(self.screen.get_size())
        fog_surface.fill((0, 0, 0))
        # 设置透明度
        fog_surface.set_alpha(self.alpha)
        self.screen.blit(fog_surface, (0, 0))

    def draw_ui(self):
        """
        根据 current_page 绘制相应页面
        """
        # 绘制背景
        self.draw_bg()
        # 动态更新
        self.bubble_sys.update()
        self.fish_sys.update()
        # 碰撞系统
        if self.current_page == "running":
            self.player_sys.update()
            crash_sys.crash_player_fish(self.fish_sys, self.player_sys)

        # 绘制遮罩层调节亮度
        self.draw_fog()

        if self.current_page == "start":
            self._start_ui()
        elif self.current_page == "settings":
            self._settings_ui()
        elif self.current_page == "pause":
            self._pause_ui()
        elif self.current_page == "running":
            self._running_ui()

        _copyright = FONT_SIZE_12.render("锦沐Python:PASSLINK", True, FONT_COLOR)
        _copyright_rect = _copyright.get_rect()
        _copyright_rect.center = (self.screen_width - self.h_margin * 2,
                                  self.screen_height - self.v_margin // 2)
        self.screen.blit(_copyright, _copyright_rect)
