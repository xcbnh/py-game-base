# -*- coding: utf-8 -*-
"""
@FileName：crash_sys.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/8/17 16:26\n
"""
from Fishing.src.sys.fish_sys import FishSys
from Fishing.src.sys.player_sys import PlayerSys


class CrashSys:
    """
    碰撞检测，获取不同的对象，编写他们的碰撞逻辑
    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def crash_player_fish(self, fish_sys: FishSys, player_sys: PlayerSys):
        """
        玩家与鱼的碰撞检测
        :param fish_sys:
        :param player_sys:
        :return:
        """
        # 检测碰撞的第一个
        if player_sys.catch_flag:
            # 被捕捉的鱼跟随玩家
            fish_sys.catch_fish.now_position = player_sys.rect.topleft
            # 判断玩家是否回到起点
            if player_sys.animate_flag is False:
                fish_sys.fishs.remove(fish_sys.catch_fish)
                fish_sys.create_fish()
                # 解除碰撞状态
                player_sys.catch_flag = False
                # 计分
                player_sys.score += fish_sys.catch_fish.score
                player_sys.level = player_sys.score // 100
            return

        for fish in fish_sys.fishs:
            if fish.rect.colliderect(player_sys.rect):
                # 判断是否可逃脱
                if fish.eacape_probaility <= 0.5:
                    # 碰撞标志位
                    fish.catch_flag = True
                    player_sys.catch_flag = True
                    fish_sys.catch_fish = fish
                    break


crash_sys = CrashSys()
