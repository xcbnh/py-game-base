# -*- coding: utf-8 -*-
"""
@FileName：fish_sys.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/8/17 16:26\n
"""
import math
import random
import pygame
from datetime import datetime
from Fishing.src.config.config import FISH_PATH_1, FISH_PATH_2, FISH_PATH_3, FISH_PATH_4
from Fishing.src.config.config import FISH_PATH_5, FISH_PATH_6, FISH_PATH_7, FISH_PATH_8
from Fishing.src.config.config import SCREEN_WIDTH, SCREEN_HEIGHT


class Fish:
    """
    定义鱼的一般属性，更新逻辑
    """

    def __init__(self, name="", img_url="", screen=None, min_eacape_probaility=0.5):
        self.screen = screen
        # 鱼类名称
        self.name = name
        # 鱼的大小 4:3
        width = random.randint(20, 30)
        self.size = (width * 4, width * 3)
        # 携带的分数
        self.score = random.randint(10, self.size[0] * self.size[1])
        # 当前位置
        self.now_position = []
        # 鱼的位置
        self.position_index = 0
        # 鱼的位置序列 20个
        self.position_list = []
        # 水平翻转图像标志
        self.flip_flag = True
        self._create_fish_move_pos_list()

        # 鱼的力量
        self.strength = 10
        # 成功逃脱概率
        self.eacape_probaility = random.uniform(min_eacape_probaility, 1)
        # 是否被玩家捕获
        self.catch_flag = False
        # 鱼的图片
        original_surface = pygame.image.load(img_url).convert_alpha()
        surface = pygame.transform.smoothscale(original_surface, self.size)
        self.surface = pygame.transform.flip(surface, self.flip_flag, False)
        # Rect 碰撞范围对象
        self.rect = self.surface.get_rect()

    def update(self):
        """
        位置更新
        """
        if self.catch_flag is False:
            # 位置更新
            self.now_position = [self.position_list[self.position_index][0], self.position_list[self.position_index][1]]
            self.position_index = (self.position_index + 1) % len(self.position_list)

        self.screen.blit(self.surface, self.now_position)
        self.rect = self.surface.get_rect()
        self.rect.topleft = self.now_position

        # pygame.draw.rect(self.screen, "red", self.rect, 2)

    def _create_fish_move_pos_list(self):
        # 生成一个随机连续位置列表
        step = random.randint(500, 900)
        x_list = [i * (2 * math.pi / step) for i in range(step)]
        sin_list = [(math.sin(x) + 1) / 2 for x in x_list]

        # 随机选择数据切片
        start = random.randint(0, step // 4)
        stop = random.randint(start + 100, step)  # 确保 stop 大于 start

        # 切片数据
        x_slice = x_list[start:stop]
        sin_slice = sin_list[start:stop]

        # 缩放到 900x600 画布
        canvas_width, canvas_height = SCREEN_WIDTH, SCREEN_HEIGHT
        min_x, max_x = min(x_slice), max(x_slice)
        slice_width = max_x - min_x

        scaled_x = [(x - min_x) / slice_width * canvas_width for x in x_slice]
        scaled_y = [(y - 0.5) * canvas_height for y in sin_slice]

        # 组合成对元组
        self.position_list = list(zip(scaled_x, scaled_y))

        if random.randint(0, 1) == 0:
            self.position_list.reverse()
            self.flip_flag = False

        self.now_position = self.position_list[0]

    def _swing(self):
        pass

    def _move(self):
        pass


class FishSys:
    """
    管理鱼群，负责创建，销毁，碰撞检测
    """

    def __init__(self, screen=None):
        self.screen = screen
        # 所有鱼
        self.fishs = []
        # 最小被捕获概率
        self.min_eacape_probaility = 0.1
        # 被捕鱼对象
        self.catch_fish = None
        # 鱼图片列表
        self.imgs = [FISH_PATH_1, FISH_PATH_2, FISH_PATH_3, FISH_PATH_4, FISH_PATH_5, FISH_PATH_6, FISH_PATH_7,
                     FISH_PATH_8]

        # 初始获取 10 条鱼
        for _ in range(10):
            self.create_fish()

    def create_fish(self):
        """
        创建鱼
        """
        fish = Fish(name=datetime.now().strftime("%y%m%d%H%M%S"), img_url=random.choice(self.imgs), screen=self.screen,
                    min_eacape_probaility=self.min_eacape_probaility)
        print(fish.eacape_probaility)
        self.fishs.append(fish)

    def update(self):
        """
        刷新
        """
        for fish in self.fishs:
            fish.update()
            self.crash_window(fish)

    def crash_window(self, fish):
        """
        检查鱼是否超出窗口，超出则销毁
        :param fish:
        """
        fish_rect = fish.rect
        window_rect = self.screen.get_rect()
        if fish_rect.right < -fish.size[0] \
                or fish_rect.left > window_rect.width \
                or fish_rect.bottom < -fish.size[1]:
            self.fishs.remove(fish)
            # 创建新的鱼
            self.create_fish()
