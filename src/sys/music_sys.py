# -*- coding: utf-8 -*-
"""
@FileName：music_sys.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/8/17 16:26\n
"""
import random
import pygame

from Fishing.src.config.config import BGM_PATH_1, BGM_PATH_2, BGM_PATH_3
from Fishing.src.config.config import BUBBLE_PATH_1, BUBBLE_PATH_2, BUBBLE_PATH_3, BUBBLE_PATH_4, BUBBLE_PATH_5
from Fishing.src.config.config import LEVEUP_PATH, MENU_PATH

pygame.mixer.init()


class MusicSys:
    """
    游戏音效控制，播放，暂停，随机挑选
    """
    _instance = None

    def __init__(self):
        self.bg_list = [BGM_PATH_1, BGM_PATH_2, BGM_PATH_3]
        self.bubble_list = [BUBBLE_PATH_1, BUBBLE_PATH_2, BUBBLE_PATH_3, BUBBLE_PATH_4, BUBBLE_PATH_5]
        self.levelup_music = pygame.mixer.Sound(LEVEUP_PATH)
        self.menu_music = pygame.mixer.Sound(MENU_PATH)
        self.bubble_music = pygame.mixer.Sound(random.choice(self.bubble_list))
        self.bg_music = pygame.mixer.Sound(random.choice(self.bg_list))

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def play_bgm(self, flag: bool):
        """
        背景音乐
        :param flag:
        """
        self.bg_music = pygame.mixer.Sound(random.choice(self.bg_list))
        if flag:
            self.bg_music.play(-1)
        else:
            self.bg_music.stop()

    def play_bubble(self, flag: bool):
        """
        气泡音效
        :param flag:
        """
        self.bubble_music = pygame.mixer.Sound(random.choice(self.bubble_list))
        if flag:
            self.bubble_music.play()
        else:
            self.bubble_music.stop()

    def play_levelup(self, flag: bool):
        """
        升级音效
        :param flag:
        """
        if flag:
            self.levelup_music.play()
        else:
            self.levelup_music.stop()

    def play_menu(self, flag: bool):
        """
        菜单音效
        :param flag:
        """
        if flag:
            self.menu_music.play()
        else:
            self.menu_music.stop()

    def volume_setting(self, volume):
        """
        音量控制 0 - 1
        :param volume:
        """
        self.bg_music.set_volume(volume)
        self.bubble_music.set_volume(volume)
        self.levelup_music.set_volume(volume)
        self.menu_music.set_volume(volume)


music_sys = MusicSys()
