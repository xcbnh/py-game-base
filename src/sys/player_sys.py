# -*- coding: utf-8 -*-
"""
@FileName：player_sys.py\n
@Description：\n
@Author：锦沐Python\n
@Time：2024/8/17 16:26\n
"""
import math
import pygame
from Fishing.src.config.config import FISH_HOOK_PATH, SCREEN_WIDTH


class PlayerSys:
    """
    玩家
    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, screen=None, start_position=(SCREEN_WIDTH//2, 10)):
        self.screen = screen
        # 开始位置
        self.start_position = start_position
        # 当前位置
        self.now_position = [start_position[0], start_position[1]+10]
        # 目标位置
        self.target_position = [start_position[0], start_position[1]+10]
        # 鱼钩
        self.fishhook = pygame.image.load(FISH_HOOK_PATH).convert_alpha()
        self.rect = self.fishhook.get_rect()
        # 绳索宽度
        self.strength = 2
        # 等级
        self.level = 1
        # 分数
        self.score = 0
        # 最高等级
        self.max_level = 1
        # 最高分
        self.max_score = 1
        self.size = [50, 50]
        # 动画
        self.num_points = 30
        # 动画结束标志
        self.animate_flag = False
        # 动画位置列表
        self.animate_pos_list = []
        self.animate_index = 0
        # 方向
        self.angle_deg = 0
        # 碰撞
        self.catch_flag = False

    def update(self):
        """
        玩家位置更新
        """
        self.max_score = max(self.max_score, self.score)
        self.max_level = max(self.max_level, self.level)

        # 开启动画
        if self.animate_flag:
            self.now_position = self.animate_pos_list[self.animate_index]
            if self.catch_flag:
                self.animate_index -= 1
            else:
                self.animate_index += 1

        # 动画结束
        if self.animate_index == 2*self.num_points - 1 or self.animate_index < 0:
            self.animate_flag = False
            self.animate_index = 0
            self.animate_pos_list.clear()

        # 鱼钩角度
        surface = pygame.transform.smoothscale(self.fishhook, self.size)
        surface = pygame.transform.rotate(surface, self.angle_deg)
        self.rect = surface.get_rect()
        self.rect.center = self.now_position
        self.screen.blit(surface, self.rect.topleft)
        pygame.draw.line(self.screen, (255, 255, 255), self.start_position,  self.now_position,  self.strength)

        # 绘制碰撞范围
        pygame.draw.rect(self.screen, "red", self.rect, 2)

    def set_animate_pos(self, pos):
        """
        计算玩家路径序列
        :param pos:
        """
        # 配置目标位置
        self.target_position[0] = pos[0]
        self.target_position[1] = pos[1]

        # 防止多次触发
        # 生成动画帧玩家位置
        if self.animate_flag is False:
            for i in range(self.num_points):
                t = i / (self.num_points - 1)
                x = int(self.start_position[0] + (self.target_position[0] - self.start_position[0]) * t)
                y = int(self.start_position[1] + (self.target_position[1] - self.start_position[1]) * t)
                self.animate_pos_list.append((x, y))

            reversed_list = list(reversed(self.animate_pos_list))
            self.animate_pos_list = self.animate_pos_list + reversed_list

        # 计算角度（弧度制）
        angle_rad = math.atan2(self.target_position[1] - self.start_position[1],
                               self.target_position[0] - self.start_position[0])
        # 将弧度转换为角度
        self.angle_deg = 90 - int(math.degrees(angle_rad) % 180)
        # 启用动画
        self.animate_flag = True

    def start_new(self):
        """
        开始新游戏
        """
        self.level = 1
        self.score = 0
        self.strength = 1
