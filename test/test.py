import math
import random
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
# 生产一个随机连续位置列表
step = 100
x_list = [i * (2 * math.pi / step) for i in range(step)]
sin_list = [(math.sin(x) + 1) / 2 for x in x_list]

# 归一化 x_list
max_x = max(x_list)
x_list = [i / max_x for i in x_list]

# 随机选择数据切片
start = random.randint(0, step // 8)
stop = random.randint(start + 1, step)  # 确保 stop 大于 start

# 切片数据
x_slice = x_list[start:stop]
sin_slice = sin_list[start:stop]

# 缩放到 900x600 画布
# 计算切片的宽度
slice_width = max(x_slice) - min(x_slice)
# 缩放到 900x600 画布
canvas_width, canvas_height = 900, 600
scaled_x = [(x - min(x_slice)) / slice_width * canvas_width for x in x_slice]
# 将 y 提升到垂直中心线附近
center_y = canvas_height / 2
scaled_y = [center_y + (y - 0.5) * canvas_height for y in sin_slice]

# 创建图形和子图网格
fig = plt.figure(figsize=(12, 8), dpi=100)
gs = gridspec.GridSpec(2, 2, height_ratios=[1, 1], width_ratios=[1, 1])

# 创建第一个子图（占用 1x3）
ax1 = plt.subplot(gs[0, :])
ax1.plot(x_list, sin_list, label='Normalized Data')
ax1.set_title('Normalized Data')

# 创建第二个子图
ax2 = plt.subplot(gs[1, 0])
ax2.plot(x_slice, sin_slice, label='Sliced Data')
ax2.set_title('Sliced Data')

# 创建第三个子图
ax3 = plt.subplot(gs[1, 1])
ax3.plot(scaled_x, scaled_y, label='Scaled Data')
ax3.set_title('Scaled Data')

plt.tight_layout()
plt.show()

